import collections
import json
import os
import requests
from git import Repo
import subprocess

def publish():
    repo = Repo('.', search_parent_directories=True)
    ref_develop = repo.commit('HEAD')
    ref_prev_develop = repo.commit('HEAD~1')
    changelist = ref_develop.diff(ref_prev_develop)
    isLibrariesModified = False


    for item in changelist:
      print("a path -> " + str(item.a_path))
      if item.a_path.startswith("stringextensions"):
        isLibrariesModified = True
        print("Found changes in libraries directory")
        break

    if isLibrariesModified:
      print("Publish new version of libraries")
      ciUserEmail = os.getenv("GITLAB_USER_EMAIL")
      print(f"KEK:: email -> {ciUserEmail}")
      subprocess.run(["git","config", "--global", "user.email", "\"Gitlab Runner\""])
      subprocess.run(["git","config", "--global", "user.name", "\"Gitlab Runner\""])

      ciReadWrite = os.getenv("CI_READ_WRITE_TOKEN")
      serverHost = os.getenv("CI_SERVER_HOST")
      projPath = os.getenv("CI_PROJECT_PATH")
      ciCommitRefName = os.getenv("CI_COMMIT_REF_NAME")
      url = f'https://gitlab-ci-token:{ciReadWrite}@{serverHost}/{projPath}.git'
    
      subprocess.run(["git","remote", "add", "-f", "b", str(url)])
      subprocess.run(["git","remote", "update"])
      subprocess.run(["git","add", "gradle.properties"])
      subprocess.run(["git","commit", "-m", "\"New version of libraries\""])
      headRef = f"HEAD:{ciCommitRefName}"
      subprocess.run(["git","push", str(url), str(headRef), "-o", "ci.skip"])
      subprocess.run(["git","remote", "rm", "b"])
    else:
      print("Changes in libraries directory was not found")

if __name__ == '__main__':
    publish()
