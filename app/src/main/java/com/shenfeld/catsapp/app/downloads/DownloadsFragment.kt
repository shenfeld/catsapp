package com.shenfeld.catsapp.app.downloads

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import com.shenfeld.catsapp.R
import com.shenfeld.catsapp.app.MainActivity
import com.shenfeld.catsapp.app.downloads.adapter.DownloadsAdapter
import com.shenfeld.catsapp.app.downloads.adapter.ImageItem
import com.shenfeld.catsapp.base.BaseFragment
import com.shenfeld.catsapp.databinding.FragmentDownloadsBinding
import com.shenfeld.catsapp.extensions.observeEvent
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.io.File
import java.time.Instant

class DownloadsFragment : BaseFragment<DownloadsViewModel, FragmentDownloadsBinding>() {
    override val layoutId: Int = R.layout.fragment_downloads
    override val viewModel: DownloadsViewModel by viewModel()
    override fun getViewBinding(): FragmentDownloadsBinding = FragmentDownloadsBinding.inflate(layoutInflater)

    private lateinit var downloadsAdapter: DownloadsAdapter
    private lateinit var photoFile: File
    private lateinit var uri: Uri

    private val getAttachmentLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { granted ->
        if (!granted) {
            navController.navigate(DownloadsFragmentDirections.actionDownloadsFragmentToMainFragment())
        } else {
            requestCameraPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    private val requestCameraPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                photoFile = File.createTempFile(
                    "Photo_${Instant.now().epochSecond}}",
                    ".jpg",
                    requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                )
                uri = FileProvider.getUriForFile(
                    requireContext(),
                    "${requireContext().applicationContext.packageName}.provider",
                    photoFile
                )
                takePhotoLauncher.launch(uri)
            }
        }

    private val takePhotoLauncher =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess ->
            hideChooser()
            try {
                if (isSuccess) {
                    (activity as? MainActivity)?.setupDownloadsBadge(isAdded = true)
                    viewModel.saveUri(uri)
                    addPhotoItem(uri)
                }
            } catch (e: NullPointerException) {
                Timber.tag("CHAT::").e(e)
            }
        }

    private val getAttachmentLauncherPhoto =
        registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri ->
            hideChooser()
            uri?.let {
                (activity as? MainActivity)?.setupDownloadsBadge(isAdded = true)
                viewModel.saveUri(it)
                addPhotoItem(it)
            }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllUris()
        setupListeners()
        setupRecyclerView()
        setupObservers()
    }

    private fun setupRecyclerView() {
        downloadsAdapter = DownloadsAdapter {
            (activity as? MainActivity)?.setupDownloadsBadge(isAdded = false)
            viewModel.deleteImage(it)
        }

        binding.rvImages.apply {
            adapter = downloadsAdapter
            layoutManager = GridLayoutManager(context, 2)
            setHasFixedSize(true)
        }
    }

    private fun setupListeners() {
        binding.fabDownload.setOnClickListener {
            showChooser()
        }

        binding.vBackground.setOnClickListener {
            hideChooser()
        }

        binding.btnCancel.setOnClickListener {
            hideChooser()
        }

        binding.btnTakePhoto.setOnClickListener {
            getAttachmentLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        binding.btnOpenPhoto.setOnClickListener {
            getAttachmentLauncherPhoto.launch(arrayOf("image/*"))
        }
    }

    private fun setupObservers() {
        viewModel.allUrisLiveData.observe(viewLifecycleOwner) {
            it.forEach { uri ->
                addPhotoItem(uri)
            }
        }

        viewModel.showLoaderLiveData.observeEvent(viewLifecycleOwner) {
            binding.progressBar.isVisible = it
        }
    }

    private fun showChooser() {
        with(binding) {
            fabDownload.hide()
            vBackground.isVisible = true
            vgChooser.isVisible = true
        }
    }

    private fun hideChooser() {
        with(binding) {
            fabDownload.show()
            vBackground.isVisible = false
            vgChooser.isVisible = false
        }
    }

    private fun addPhotoItem(uri: Uri) {
        downloadsAdapter.add(ImageItem(uri = uri))
    }

    override fun onBackPressed(): Boolean {
        return true
    }
}