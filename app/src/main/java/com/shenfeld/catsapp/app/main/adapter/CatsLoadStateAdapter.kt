package com.shenfeld.catsapp.app.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.catsapp.databinding.ItemLoadStateBinding

class CatsLoadStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<CatsLoadStateAdapter.LoadStateVH>() {
    override fun onBindViewHolder(holder: LoadStateVH, loadState: LoadState) {
        val progress = holder.loadStateBinding.loadStateProgress
        val btnRetry = holder.loadStateBinding.loadStateRetry
        val tvMessage = holder.loadStateBinding.loadStateErrorMessage

        btnRetry.isVisible = loadState !is LoadState.Loading
        tvMessage.isVisible = loadState !is LoadState.Loading
        progress.isVisible = loadState is LoadState.Loading

        if (loadState is LoadState.Error) {
            tvMessage.text = loadState.error.localizedMessage
        }

        btnRetry.setOnClickListener {
            retry.invoke()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoadStateVH {
        return LoadStateVH(
            loadStateBinding = ItemLoadStateBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class LoadStateVH(val loadStateBinding: ItemLoadStateBinding) :
        RecyclerView.ViewHolder(loadStateBinding.root)
}