package com.shenfeld.catsapp.app.cat_detailed

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.navArgs
import com.shenfeld.catsapp.R
import com.shenfeld.catsapp.base.BaseFragment
import com.shenfeld.catsapp.databinding.FragmentCatDetailedBinding
import com.shenfeld.catsapp.extensions.copyToClipboard
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.ext.android.viewModel


class CatDetailedFragment : BaseFragment<CatDetailedViewModel, FragmentCatDetailedBinding>() {
    override val layoutId: Int = R.layout.fragment_cat_detailed
    override val viewModel: CatDetailedViewModel by viewModel()
    override fun getViewBinding(): FragmentCatDetailedBinding = FragmentCatDetailedBinding.inflate(layoutInflater)

    private val args: CatDetailedFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupListeners()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    private fun setupListeners() {
        binding.fabCopy.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(args.cat.wikiUrl))
            startActivity(browserIntent)
        }

        binding.tvDescription.setOnLongClickListener {
            context?.copyToClipboard(
                clipboardText = "${args.cat.name}\n${args.cat.description}\n\nMore info: ${args.cat.wikiUrl}",
                toastMessageText = "Successful copied",
                withVibration = true
            )
            true
        }
    }

    private fun setupView() {
        Picasso.get()
            .load(args.cat.imageUrl)
            .placeholder(ContextCompat.getDrawable(context!!, R.drawable.ic_downloaded_unselect)!!)
            .into(binding.ivCat)

        binding.tvDescription.text = args.cat.description
        binding.mainToolbar.title = args.cat.name
    }
}