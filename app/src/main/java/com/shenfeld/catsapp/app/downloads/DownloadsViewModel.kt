package com.shenfeld.catsapp.app.downloads

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.shenfeld.catsapp.app.downloads.adapter.ImageItem
import com.shenfeld.catsapp.base.BaseViewModel
import com.shenfeld.catsapp.data.repositories.AppRepository
import com.shenfeld.catsapp.extensions.Event
import com.shenfeld.catsapp.extensions.asEvent
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DownloadsViewModel(
    private val appRepository: AppRepository,
) : BaseViewModel() {
    private val _allUrisLiveData = MutableLiveData<List<Uri>>()
    val allUrisLiveData: LiveData<List<Uri>> = _allUrisLiveData

    val showLoaderLiveData = MutableLiveData<Event<Boolean>>()

    fun saveUri(uri: Uri) {
        viewModelScope.launch {
            appRepository.saveUri(uri = uri)
        }
    }

    fun deleteImage(item: ImageItem) {
        viewModelScope.launch {
            appRepository.deleteImage(item = item)
        }
    }

    fun getAllUris() {
        showLoaderLiveData.value = true.asEvent()
        viewModelScope.launch {
            _allUrisLiveData.value = appRepository.getAllUris()
            delay(1000L)
            showLoaderLiveData.value = false.asEvent()
        }
    }
}