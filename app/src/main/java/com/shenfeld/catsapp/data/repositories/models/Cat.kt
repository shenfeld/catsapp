package com.shenfeld.catsapp.data.repositories.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.shenfeld.catsapp.base.RvModelBase
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "cat")
data class Cat(
    @PrimaryKey var id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "imageUrl") val imageUrl: String,
    @ColumnInfo(name = "temperament") val temperament: String,
    @ColumnInfo(name = "origin") val origin: String,
    @ColumnInfo(name = "wikiUrl") val wikiUrl: String,
    @ColumnInfo(name = "countryCode") val countryCode: String,
    @ColumnInfo(name = "energyLevel") val energyLevel: Int,
    @ColumnInfo(name = "grooming") val grooming: Int,
    @ColumnInfo(name = "hypoallergenic") val hypoallergenic: Int,
    @ColumnInfo(name = "hairless") val hairless: Int,
    @ColumnInfo(name = "inFavorites") var inFavorites: Boolean = false,
) : Parcelable, RvModelBase(ItemType.CAT.ordinal) {
    enum class ItemType {
        CAT
    }
}