package com.shenfeld.catsapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shenfeld.catsapp.data.repositories.models.Image
import com.shenfeld.catsapp.data.repositories.models.Cat

@Database(entities = [Cat::class, Image::class], version = 3)
abstract class AppDatabase : RoomDatabase() {
    abstract fun catDao(): CatDao
}