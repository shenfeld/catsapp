package com.shenfeld.catsapp.data.repositories

import android.net.Uri
import androidx.core.net.toUri
import com.shenfeld.catsapp.app.downloads.adapter.ImageItem
import com.shenfeld.catsapp.data.api.CatsAPI
import com.shenfeld.catsapp.data.db.AppDatabase
import com.shenfeld.catsapp.data.repositories.models.Cat
import com.shenfeld.catsapp.data.repositories.models.Image
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class AppRepository(
    private val appStorage: AppStorage,
    private val catsAPI: CatsAPI,
    private val appDatabase: AppDatabase,
) {
    suspend fun getCats(page: Int, count: Int): List<Cat> = withContext(Dispatchers.IO) {
        val favoriteCats = appDatabase.catDao().getAll().map { it.id }
        catsAPI.getCats(page = page, count = count).map {
            Cat(
                id = it.id,
                name = it.name,
                description = it.description,
                imageUrl = it.image?.url ?: "non url",
                temperament = it.temperament,
                origin = it.origin,
                wikiUrl = it.wikiUrl ?: "https://en.wikipedia.org/wiki/Main_Page",
                countryCode = it.countryCode,
                energyLevel = it.energyLevel,
                grooming = it.grooming,
                hypoallergenic = it.hypoallergenic,
                hairless = it.hairless,
                inFavorites = favoriteCats.contains(it.id)
            )
        }
    }

    suspend fun getAllFavorites(): List<Cat> = withContext(Dispatchers.IO) {
        appDatabase.catDao().getAll()
    }

    suspend fun addToFavorites(cat: Cat) = withContext(Dispatchers.IO) {
        appDatabase.catDao().insert(cat)
    }

    suspend fun deleteFromFavorites(cat: Cat) = withContext(Dispatchers.IO) {
        appDatabase.catDao().delete(cat = cat)
    }

    suspend fun saveLastFavBadgeCount(count: Int) = withContext(Dispatchers.IO) {
        appStorage.favBadgeCount = count
    }

    suspend fun saveLastDownloadsBadgeCount(count: Int) = withContext(Dispatchers.IO) {
        appStorage.downloadsBadgeCount = count
    }

    suspend fun getLastDownloadsBadgeCount(): Int = withContext(Dispatchers.IO) {
        appStorage.downloadsBadgeCount
    }

    suspend fun getLastSavedBadgeCount(): Int = withContext(Dispatchers.IO) {
        appStorage.favBadgeCount
    }

    suspend fun saveUri(uri: Uri) = withContext(Dispatchers.IO) {
        val uriString = uri.toString()
        appDatabase.catDao().insertUri(item = Image(id = 0, image = uriString))
    }

    suspend fun getAllUris(): List<Uri> = withContext(Dispatchers.IO) {
        val listStringUris = appDatabase.catDao().getAllUris()
        listStringUris.map {
            it.image.toUri()
        }
    }

    suspend fun deleteImage(item: ImageItem) = withContext(Dispatchers.IO) {
        appDatabase.catDao().deleteUri(image = appDatabase.catDao().findImage(uri = item.uri.toString()))
    }
}
