package com.shenfeld.catsapp.data.api

import com.shenfeld.catsapp.data.api.repsonses.CatResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CatsAPI {
    @GET("breeds")
    suspend fun getCats(
        @Query("page") page: Int,
        @Query("limit") count: Int,
    ) : List<CatResponse>
}