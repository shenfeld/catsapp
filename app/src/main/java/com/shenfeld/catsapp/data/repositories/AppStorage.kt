package com.shenfeld.catsapp.data.repositories

import android.content.SharedPreferences
import com.shenfeld.catsapp.extensions.SharedPreferencesDelegate

class AppStorage(private val sharedPreferences: SharedPreferences) {

    companion object {
        private const val FAV_BADGE_COUNT = "AppStorage.fav_badge_count"
        private const val DOWNLOADS_BADGE_COUNT = "AppStorage.downloads_badge_count"
    }

    var favBadgeCount: Int by SharedPreferencesDelegate(sharedPreferences, FAV_BADGE_COUNT, 0)
    var downloadsBadgeCount: Int by SharedPreferencesDelegate(sharedPreferences, DOWNLOADS_BADGE_COUNT, 0)
}
