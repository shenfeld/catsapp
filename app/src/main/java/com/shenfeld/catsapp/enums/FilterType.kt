package com.shenfeld.catsapp.enums

enum class FilterType(val value: String) {
    SELECT_FILTER("Select filter"),
    ALL("All"),
    HAIRLESS("Hairless"),
    WITH_HAIR("With hair"),
    HYPOALLERGENIC("Hypoallergenic"),
    MOST_ENERGETIC("Most energetic");
}