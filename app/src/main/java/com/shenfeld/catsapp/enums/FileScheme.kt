package com.shenfeld.catsapp.enums

enum class FileScheme(val value: String) {
    FILE("file"),
    CONTENT("content");

    companion object {
        fun getTypeByValue(value: String): FileScheme {
            return values().find { it.value.equals(value, true) } ?: FILE
        }
    }
}