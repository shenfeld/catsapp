package com.shenfeld.catsapp.base

sealed class Result<out T: Any> {
    data class Loading(val isLoading: Boolean): Result<Nothing>()
    data class Success<out T: Any>(val data: T) : Result<T>()
    data class Error(val error: Exception) : Result<Nothing>()
}