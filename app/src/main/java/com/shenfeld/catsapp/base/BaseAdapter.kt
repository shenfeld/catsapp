package com.shenfeld.catsapp.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<ModelT : RvModelBase, ViewHolderT : RecyclerView.ViewHolder> :
    RecyclerView.Adapter<ViewHolderT>() {
    companion object {
        const val PAYLOAD_NO_ANIMATION = -1
    }

    protected interface Bindable<in T : RvModelBase> {
        fun bind(item: T)
    }

    private val _items = mutableListOf<ModelT>()

    protected var onClick: ((ModelT) -> Unit)? = null

    override fun getItemCount(): Int = _items.size

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position) ?: throw IllegalArgumentException()
        return item.itemType
    }

    fun setOnclickListener(onClick: ((ModelT) -> Unit)) {
        this.onClick = onClick
    }

    fun isEmpty(): Boolean = _items.isEmpty()

    fun getItems(): List<ModelT> = _items

    fun getItem(position: Int): ModelT? = _items.getOrNull(position)

    fun <T : ModelT> add(item: T, withoutNotify: Boolean = false) {
        add(item, itemCount, withoutNotify)
    }

    fun <T : ModelT> add(item: T, startPosition: Int, withoutNotify: Boolean = false) {
        var position = startPosition
        if (position > itemCount) {
            position = itemCount
        }
        val initialSize = itemCount
        _items.add(position, item)
        if(!withoutNotify) {
            if (initialSize == 0) {
                notifyDataSetChanged()
            } else {
                notifyItemInserted(position)
            }
        }
    }

    fun <T : ModelT> addAll(items: List<T>, withoutNotify: Boolean = false) {
        addAll(items, itemCount, withoutNotify)
    }


    fun <T : ModelT> addAll(items: List<T>, startPosition: Int, withoutNotify: Boolean = false) {
        if (items.isEmpty()) {
            return
        }
        val initialSize = itemCount
        var position = startPosition
        if (position > itemCount) {
            position = itemCount
        }
        _items.addAll(position, items)
        if (!withoutNotify) {
            if (initialSize == 0) {
                notifyDataSetChanged()
            } else {
                notifyItemRangeInserted(position, items.size)
            }
        }
    }

    fun <T : ModelT> replace(item: T, position: Int, withoutNotify: Boolean = false): ModelT? {
        return replace(item, position, PAYLOAD_NO_ANIMATION, withoutNotify)
    }

    fun <T : ModelT> replace(
        item: T,
        position: Int,
        payload: Any?,
        withoutNotify: Boolean = false
    ): ModelT? {
        var index = position
        if (index > itemCount) {
            index = itemCount
        }
        val oldItem: ModelT = _items.removeAt(index)
        _items.add(index, item)
        if(!withoutNotify) {
            if (payload == null) {
                notifyItemChanged(position)
            } else {
                notifyItemChanged(position, payload)
            }
        }
        return oldItem
    }

    fun <T : ModelT> replaceWithoutRemoving(
        item: T,
        position: Int,
        payload: Any?,
        withoutNotify: Boolean = false
    ): ModelT? {
        var index = position
        if (index > itemCount) {
            index = itemCount
        }
        val oldItem: ModelT = _items.set(index, item)
        if(!withoutNotify) {
            if (payload == null) {
                notifyItemChanged(index)
            } else {
                notifyItemChanged(index, payload)
            }
        }
        return oldItem
    }

    fun <T : ModelT> set(item: T, position: Int, payload: Any?, withoutNotify: Boolean = false) {
        var index = position
        if (index > itemCount) {
            index = itemCount
        }
        _items[index] = item
        if(!withoutNotify) {
            if (payload == null) {
                notifyItemChanged(index)
            } else {
                notifyItemChanged(index, payload)
            }
        }
    }

    fun <T : ModelT> replaceAll(items: List<T>, withoutNotify: Boolean = false) {
        clear(withoutNotify)
        addAll(items, withoutNotify)
    }

    fun <T : ModelT> replaceAllWithoutUpdate(items: List<T>) {
        _items.clear()
        _items.addAll(items)
    }

    fun clear(withoutNotify: Boolean = false) {
        val size: Int = itemCount
        _items.clear()
        if (!withoutNotify) {
            notifyItemRangeRemoved(0, size)
        }
    }

    fun remove(position: Int, withoutNotify: Boolean = false ): ModelT? {
        if (position < 0 || position >= itemCount) {
            return null
        }
        val removed: ModelT = _items.removeAt(position)
        if(!withoutNotify) {
            if (_items.isEmpty() || position == 0) {
                notifyDataSetChanged()
            } else {
                notifyItemRemoved(position)
            }
        }
        return removed
    }
}
