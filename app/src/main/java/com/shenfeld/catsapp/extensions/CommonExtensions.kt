package com.shenfeld.catsapp.extensions

import android.annotation.SuppressLint
import android.app.Application
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.os.Vibrator
import android.widget.Toast
import androidx.annotation.RawRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.gson.Gson
import org.koin.core.context.GlobalContext

inline fun <reified T> fromJsonToObject(@RawRes idRes: Int): T? {
    return try {
        val gson: Gson = GlobalContext.get().get()
        val app: Application = GlobalContext.get().get()
        val json = app.resources.openRawResource(idRes).use { it.bufferedReader().readText() }
        return gson.fromJson(json, T::class.java)
    } catch (e: Exception) {
        null
    }
}

inline fun <reified T> fromJsonToObject(json: String?): T? {
    return try {
        if (json.isNullOrEmpty()) return null
        val gson: Gson = GlobalContext.get().get()
        return gson.fromJson(json, T::class.java)
    } catch (e: Exception) {
        null
    }
}

inline fun <reified T> fromObjectToJson(value: T?): String {
    return try {
        if (value == null) return ""
        val gson: Gson = GlobalContext.get().get()
        return gson.toJson(value)
    } catch (e: Exception) {
        ""
    }
}

fun <T> T.asEvent(): Event<T> {
    return Event(this)
}

inline fun <T> LiveData<Event<T>>.observeEvent(
    owner: LifecycleOwner,
    crossinline observer: (value: T) -> Unit
) {
    this.observe(owner, Observer {
        it.getOnce { value ->
            observer.invoke(value)
        }
    })
}

inline fun <T> Event<T>.getOnce(block: (T) -> Unit) {
    block(getValue() ?: return)
}

fun Context.copyToClipboard(
    clipboardText: CharSequence,
    toastMessageText: String? = null,
    withVibration: Boolean = false,
    vibrationDuration: Long = 50
) {
    val clipboard = ContextCompat.getSystemService(this, ClipboardManager::class.java)
    val clip = ClipData.newPlainText("", clipboardText)
    clipboard?.setPrimaryClip(clip)

    toastMessageText?.let { text ->
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    if (withVibration) {
        vibrate(
            vibrationDuration = vibrationDuration,
            vibrationEffect = VibrationEffect.EFFECT_TICK
        )
    }
}

enum class VibrationEffect(val value: Int) {
    EFFECT_TICK(android.os.VibrationEffect.EFFECT_TICK),
    EFFECT_CLICK(android.os.VibrationEffect.EFFECT_CLICK),
    EFFECT_DOUBLE_CLICK(android.os.VibrationEffect.EFFECT_DOUBLE_CLICK),
    EFFECT_HEAVY_CLICK(android.os.VibrationEffect.EFFECT_HEAVY_CLICK);
}

@SuppressLint("MissingPermission")
fun Context.vibrate(
    vibrationDuration: Long = 50,
    vibrationEffect: VibrationEffect = VibrationEffect.EFFECT_TICK
) {
    (getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator)?.let { vibrator ->
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(
                android.os.VibrationEffect.createOneShot(
                    vibrationDuration,
                    vibrationEffect.value
                )
            )
        } else {
            vibrator.vibrate(vibrationDuration)
        }
    }
}
