package com.shenfeld.catsapp.extensions

import android.content.SharedPreferences

@Suppress("UNCHECKED_CAST")
fun <T> SharedPreferences.readValue(key: String, defaultValue: T): T =
    when (defaultValue) {
        is String -> {
            getString(key, defaultValue) as T
        }
        is Long -> {
            getLong(key, defaultValue) as T
        }
        is Boolean -> {
            getBoolean(key, defaultValue) as T
        }
        is Float -> {
            getFloat(key, defaultValue) as T
        }
        is Double -> {
            if (contains(key)) {
                java.lang.Double.longBitsToDouble(getLong(key, 0))
            } else {
                defaultValue
            } as T
        }
        is Int -> {
            getInt(key, defaultValue) as T
        }
        else -> throw IllegalArgumentException("Type is not supported!")
    }

fun <T> SharedPreferences.saveValue(key: String, newValue: T): T =
    edit().apply {
        when (newValue) {
            is String -> {
                putString(key, newValue)
            }
            is Long -> {
                putLong(key, newValue)
            }
            is Boolean -> {
                putBoolean(key, newValue)
            }
            is Float -> {
                putFloat(key, newValue)
            }
            is Double -> {
                putLong(key, java.lang.Double.doubleToLongBits(newValue))
            }
            is Int -> {
                putInt(key, newValue)
            }
            else -> throw IllegalArgumentException("Type is not supported!")
        }
    }.apply().let {
        newValue
    }

