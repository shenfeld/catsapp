package com.shenfeld.catsapp.di

import com.shenfeld.catsapp.app.MainActivityViewModel
import com.shenfeld.catsapp.app.cat_detailed.CatDetailedViewModel
import com.shenfeld.catsapp.app.downloads.DownloadsViewModel
import com.shenfeld.catsapp.app.favorites.FavoritesViewModel
import com.shenfeld.catsapp.app.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    viewModel { MainViewModel(appRepository = get()) }
    viewModel { FavoritesViewModel(appRepository = get()) }
    viewModel { DownloadsViewModel(appRepository = get()) }
    viewModel { CatDetailedViewModel() }
    viewModel { MainActivityViewModel(appRepository = get()) }
}

